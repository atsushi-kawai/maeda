<?php

// generic interface between backend scripts to frontend page.
//   input: 'pgname' and 'args' in plain text.
//   output: array of objs in JSON format.


$jsonstr = file_get_contents('php://input');
$arg = json_decode($jsonstr);
$bindir = '/home/fd-tech/src/maeda/bin/';
$cmd = $bindir . $arg->pgname . ' ' . $arg->args;
exec($cmd, $results, $status);

$ary = array();
foreach ($results as $res) {
    $ary[] = $res;
}
$jsonstr = '[' . implode(',', $ary) . ']';
echo $jsonstr;

?>
