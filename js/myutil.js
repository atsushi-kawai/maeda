window.onerror = function(msg, url, line) {
    alert(msg + "\n" + url + ":" + line);
};

!function($) {
    "use strict";

    let MyUtil = function() {
        try {
            if ("undefined"== typeof document) return false;
            this.pgParse = 'parse.py';
            this.mega = 1024 * 1024;
            this.giga = 1024 * 1024 * 1024;
            this.people = null;
        }
        catch (e) {
            console.log(e);
        }
        this.care4IE();
    };

    MyUtil.prototype = {

        // polyfills
        care4IE : function() {
            // Math.log10(): not supported by IE.
            Math.log10 = Math.log10 || function(x) {
                return Math.log(x) * Math.LOG10E;
            }

            // Math.trunc(): not supported by IE.
            Math.trunc = Math.trunc || function(x) {
                return x < 0 ? Math.ceil(x) : Math.floor(x);
            }

            // MouseEvent(): not supported by IE11 or earlier.
            if (typeof window.MouseEvent != 'function') {
                let MouseEvent = function (eventType, params) {
                    params = params || { bubbles: false, cancelable: false };
                    let mouseEvent = document.createEvent('MouseEvent');
                    mouseEvent.initMouseEvent(eventType, params.bubbles, params.cancelable,
                                              window, 0, 0, 0, 0, 0,
                                              false, false, false, false, 0, null);
                    return mouseEvent;
                }
                MouseEvent.prototype = Event.prototype;
                window.MouseEvent = MouseEvent;
            }

            // Array.includes method not supported by IE11 or earlier.
            if (!Array.prototype.includes) {
                Object.defineProperty(Array.prototype, 'includes', {
                    value: function(searchElement, fromIndex) {
                        if (this == null) {
                            throw new TypeError('"this" is null or not defined');
                        }
                        let o = Object(this);
                        let len = o.length >>> 0;
                        if (len === 0) {
                            return false;
                        }
                        let n = fromIndex | 0;
                        let k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
                        while (k < len) {
                            if (o[k] === searchElement) {
                                return true;
                            }
                            k++;
                        }
                        return false;
                    }
                });
            }
        },

        byte2human: function(num, join, belowbin) {
            if (!belowbin) belowbin = 0; // digits below binary point
            let suf = ['', 'K', 'M', 'G', 'T', 'P'];
            let i = 0;
            while (num >= 1024 && i < suf.length - 1) {
                num /= 1024.0;
                i += 1;
            }
            let ret;
            if (join) {
                if (isNaN(num) || num == null) {
                    ret = null;
                }
                else {
                    ret = (+num).toFixed(belowbin) + suf[i];
                }
            }
            else {
                ret = { "val": num, "suf": suf[i] };
            }
            return ret;
        },

        // similar to byte2human but aligned not by 1024^n but by 1000^n.
        byte2engineer: function(num, join, belowbin) {
            if (!belowbin) belowbin = 0; // digits below binary point
            let suf = ['', 'k', 'm', 'g', 't', 'p'];
            let i = 0;
            while (num >= 1000 && i < suf.length - 1) {
                num /= 1000.0;
                i += 1;
            }
            let ret;
            if (join) {
                if (isNaN(num) || num == null) {
                    ret = null;
                }
                else {
                    ret = num.toFixed(belowbin) + suf[i];
                }
            }
            else {
                ret = { "val": num, "suf": suf[i] };
            }
            return ret;
        },

        human2byte: function(sizestr) {
            let suf2mul = {
                'K': 1024,
                'M': 1024 * 1024,
                'G': 1024 * 1024 * 1024,
                'T': 1024 * 1024 * 1024 * 1024,
                'P': 1024 * 1024 * 1024 * 1024 * 1024,
            };
            let m = sizestr.match(/\s*(\d+(?:[.]\d*)?)(\w*)/);
            let num = m[1];
            let suf = m[2];
            if (suf) {
                num *= suf2mul[suf];
            }
            return num;
        },

        /*
         * convert seconds to minutes, hours, days, weeks or years.
         */

        sec2human: function(numsec, join, belowbin) {
            let suf = 'sec', ret, days, num;
            if (!belowbin) belowbin = 0; // digits below binary point

            if (numsec > 60) {
                num = numsec / 60;
                suf = 'min';
            }
            if (numsec > 60 * 60) {
                num = numsec / 60 / 60;
                suf = 'hr';
            }
            if (numsec > 60 * 60 * 24) {
                num = numsec / 60 / 60 / 24;
                suf = 'day';
            }
            if (numsec > 60 * 60 * 24 * 7) {
                num = numsec / 60 / 60 / 24 / 7;
                suf = 'week';
            }
            if (numsec > 60 * 60 * 24 * 365) {
                num = numsec / 60 / 60 / 24 / 365;
                suf = 'yr';
            }

            if (join) {
                if (isNaN(num) || num == null) {
                    ret = '<1' + suf;
                }
                else {
                    ret = (+num).toFixed(belowbin) + suf;
                }
            }
            else {
                ret = { "val": num, "suf": suf };
            }
            return ret;
        },

        number_with_comma: function(num) {
            return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },

        sort_hash_by_key: function(src, key, dir) {
            let rev = -1;
            if (dir == "ASC") {
                let rev = +1;
            }
            // first convert Obj 'src' into Array 'dst', then sort.
            let dst = [];
            for (let i = 0; i < src.length; i++) {
                dst[i] = src[i];
            }
            switch (key) {
            case "username": // string sort
            case "jobname":
                dst.sort(function(a, b) {
                    if (a[key] < b[key]) return -1 * rev;
                    if (a[key] > b[key]) return +1 * rev;
                    return 0;
                });
                break;
            default: // integer sort
                dst.sort(function(a, b) {
                    if (parseInt(a[key], 10) < parseInt(b[key], 10)) return -1 * rev;
                    if (parseInt(a[key], 10) > parseInt(b[key], 10)) return +1 * rev;
                    return 0;
                });
            }
            return dst;
        },

        // check if the web browser is MS-Edge or not.
        isEdge: function() {
            let isIE = /*@cc_on!@*/false || !!document.documentMode;

            // Edge 20+
            return (!isIE && !!window.StyleMedia);
        },

        query_to_backend: function(pgname, args, callback) {
            let my = this;
            let $dfd = $.Deferred();
            $.ajax({
                url: "./php/backend.php",
                data: JSON.stringify({
                    pgname: pgname,
                    args: args,
                }),
                type: "POST",
                processData: false,
                contentType: false,
                dataType: "json",
                timeout: 200000,
            }).then(function(res) {
                let ret = null;
                if (callback) {
                    let ret = callback(res[0]);
                }
                if (ret) {
                    ret.then(function(res1) {
                        $dfd.resolve(res1); // nobody using res1 for now but just in case.
                    });
                }
                else { // no callback given or callback is a synchronous function.
                    $dfd.resolve(res[0]);
                }
            }, function(jqXHR, status, thrown) {
                console.log("pgname:", pgname, " jqXHR:", jqXHR, " status:", status, " thrown:", thrown);
                $dfd.reject(status);
            });
            return $dfd.promise();
        },

        msec2human: function(msec) {
            let human;
            let s = Math.round(msec / 1000.0);
            if (s < 60) return (s + 's');
            let m = Math.trunc(s / 60);
            s = s % 60;
            if (m < 60) return (m + 'm ' + s + 's');
            let h = Math.trunc(m / 60);
            m = m % 60;
            if (h < 24) return (h + 'h ' + m + 'm');
            let d = Math.trunc(h / 24);
            h = h % 24;
            return (d + 'd ' + h + 'h');
        },

        date_to_string: function(date, format) {
            let monthname = [
                'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
            ];
            let dayofweekname = [
                'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',
            ];

            if (!format) format = '%Y/%m/%d %H:%M:%S';

            format = format.replace(/%Y/g, date.getFullYear());
            format = format.replace(/%m/g, ('0' + (date.getMonth() + 1)).slice(-2));
            format = format.replace(/%d/g, ('0' + date.getDate()).slice(-2));
            format = format.replace(/%H/g, ('0' + date.getHours()).slice(-2));
            format = format.replace(/%M/g, ('0' + date.getMinutes()).slice(-2));
            format = format.replace(/%S/g, ('0' + date.getSeconds()).slice(-2));
            format = format.replace(/%b/g, monthname[date.getMonth()]);
            format = format.replace(/%a/g, dayofweekname[date.getDay()]);
            return format;
        },

        seconds_ago: function(sec) {
            let now = new Date();
            let date = new Date();
            date.setTime(now.getTime() - sec * 1000);
            return date;
        },

        // truncate src to the nearest dsrc aligned value smaller than src.
        align_floor: function(src, dsrc) {
            let dst = (Math.floor(src / dsrc)) * dsrc;
            //    console.log("src:" + src + " dst:" + dst + " dsrc:" + dsrc);
            return dst;
        },

        // round up src to the nearest dsrc aligned value larger than src.
        align_ceil: function(src, dsrc) {
            let dst = (Math.ceil(src / dsrc)) * dsrc;
            //    console.log("src:" + src + " dst:" + dst);
            return dst;
        },


        isempty: function(obj) {
            return Object.keys(obj).length === 0 ? true : false;
        },

        capitalize: function(str) {
            return str.charAt(0).toUpperCase() + str.slice(1);
        },

        dialog: function($panel, title, body) {
            let panelname = $panel.find('.mypanel-title span').html().trim();
            $('#mypanel-help-placeholder')
                .html(body)
                .dialog({
                    width: 768,
                    title: panelname + ':' + title,
                    modal: true,
                    show: "fade",
                    hide: "fade",
                    buttons: [{
                        text: "Close",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }],
                });
        },

        dialog_yes_no: function($panel, title, body, btnfunc) {
            let panelname = $panel.find('.mypanel-title span').html().trim();
            $('#mypanel-help-placeholder')
                .html(body)
                .dialog({
                    width: 512,
                    title: panelname + ':' + title,
                    modal: true,
                    show: "fade",
                    hide: "fade",
                    buttons: [
                        {
                            text: "Yes",
                            click: function() {
                                btnfunc();
                                $(this).dialog("close");
                            },
                        },
                        {
                            text: "No",
                            click: function() {
                                $(this).dialog("close");
                            },
                        }
                    ],
                });
        },
    };
    window.MyUtil = MyUtil;
}(jQuery)
