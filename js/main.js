!function($) {
    "use strict";
    let Bar = null;
    window.My = new MyUtil();

    async function barchart_loader(src) {
        return src;
    }

    function load_log() {
        let $dfd = $.Deferred();
        let args = '';
        My.query_to_backend(My.pgParse, args, null).then(function (log) {
            $dfd.resolve(log);
        });
        return $dfd.promise();
    }

    function restruct_log(logs) {
        let res = {};
        for (let log of logs) {
            let yyyy = log.date.slice(0, 4);
            let mmdd = log.date.slice(5, 10);
            let hh = +log.time;
            if (!(yyyy in res)) res[yyyy] = {};
            if (!(mmdd in res[yyyy])) res[yyyy][mmdd] = [];
            res[yyyy][mmdd].push({ time: log.time, color: log.color});
        }
        let resarray = [];
        for (let yyyy in res) {
            let dates = [];
            for (let mmdd in res[yyyy]) {
                dates.push({
                    mmdd: mmdd,
                    hist: res[yyyy][mmdd],
                });
            }
            resarray.push({
                yyyy: yyyy,
                dates: dates,
            });
        }
        return resarray;
    }

    async function init_var() {
        let logs = await load_log();
        let flaghist = restruct_log(logs);
        let h = '';
        h += '<h1>Cape Maeda Sea Condition</h1>';
        h += '<a href="index.html">raw data</a>';
        for (let year of flaghist.sort(function(a, b) { return a > b ? +1 : -1; } )) {
            h += '<div>';
            h += `<h2>${year.yyyy}</h2>`;
            h += '<div id="cont-' + year.yyyy + '">';
            h += '</div>';
            h += '</div>';
        }
        $('#container').html(h);

        for (let year of flaghist) {
            let param = {
                height: 220,
                barwidth: 2,
                yyyy: year.yyyy,
                padding: { top: 10, right: 40, bottom: 20, left: 40, },
                loader: function() { return barchart_loader(year.dates); } ,
            };
            Bar = new Barchart('#cont-' + year.yyyy, param);
        }
    }

    $(function init() {
        init_var();
    });
}(jQuery);
