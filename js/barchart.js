!function($) {
    "use strict";
    let Barchart = function(containerstr, params) {
        try {
            let required = ['loader', ]; // required params
            this.container = document.querySelector(containerstr);
            this.container.className = "barchart";
            this.container.str = containerstr;

            // default params
            this.tstart = 6;
            this.tend = 18;
            this.barwidth = 1;
            this.height = 320;
            this.padding = { top: 0, right: 0, bottom: 0, left: 0, };
            if (typeof params === 'undefined') {
                throw('Barchart(container_id, param) requires param.');
            }
            for (let key in params) {
                this[key] = params[key];
            }
            for (let i = 0; i < required.length; i++) {
                if (!this[required[i]]) {
                    throw('Barchart(container_id, param) requires param.' + required[i] + '.');
                }
            }
            
            // init vars
            this.height0 = this.height - this.padding.top - this.padding.bottom;
            this.container.style.height = this.height + 'px';

            // initial plot
            this.renew_plot();
        }
        catch (e) {
            console.log(e);
        }
    };
    
    Barchart.prototype = {
        parse_date: d3.timeParse('%Y/%m/%d'),

        set_loader: function(loader) {
            this.loader = loader;
        },

        load_data: function() {
            let me = this;
            let $cont = $(me.container.str);
            let promise = this.loader().then(function(res){
                me.data = res;
                let ndata = me.data.length;
                me.width0 = me.barwidth * 365;
                me.width = me.width0 + me.padding.left + me.padding.right;
                me.container.style.width = me.width + 'px';

                let dayfirst = new Date(+me.yyyy, 0, 1);
                let daylast = new Date(+me.yyyy + 1, 0, 1);
                me.xdomain = [dayfirst, daylast];
                $cont.html('');
            });
            return promise;
        },

        renew_plot: function() {
            let me = this;
            this.load_data().then(function() {
                me.plot();
            });
        },

        reload: function(params) {
            if (typeof params !== 'undefined') {
                for (let key in params) {
                    this[key] = params[key];
                }
            }
            this.renew_plot(this);
        },
        
        plot: function() {
            let me = this;
            let divid = me.container.str;
            let bp = 2; // bar width padding

            d3.select(divid).select("svg").remove();

            let svg = d3.select(divid).append("svg")
                .attr("width", me.width)
                .attr("height",me.height)
                .append("g")
                .attr("class", "base")
                .attr("transform", "translate(" + me.padding.left + ", " + me.padding.top + ")");

            let xscale = d3.fisheye.scale(d3.scaleTime)
                .distortion(0)
                .domain(me.xdomain)
                .range([0, me.width0])

            svg.on("mousemove", function() {
                let mouse = d3.mouse(this);
                xscale.distortion(3.0);
                xscale.focus(mouse[0]);    
                svg.select('.x-axis').call(xaxis);
                redraw_rects();
            });


            svg.on("mouseleave", function() {
                let mouse = d3.mouse(this);
                let mx = mouse[0], my = mouse[1];
                if (my < 0 || 200 < my || mx < 0 || 700 < mx) {
                    xscale.distortion(0);
                    redraw_rects();
                }
            });

            function redraw_rects() {
                for (let rect of me.rects) {
                    let r = rect.svgobj;
                    let d0 = mmdd2date(rect.day.mmdd);
                    r.attr("x", xscale(d0));
                    let d1 = new Date();
                    d1.setDate(d0.getDate() + 1);
                    let w = xscale(d1) - xscale(d0);
                    w = w > 10 ? 10 : w;
                    r.attr("width", function() { return w; });
                }
            }

/*
            let xscale = d3.scaleTime()
                .domain(me.xdomain)
                .range([0, me.width0])
*/
            let xaxis = d3.axisBottom()
                .scale(xscale)
                .ticks(d3.timeMonth, 1)
                .tickFormat(d3.timeFormat('%b %d'))
                .tickSize(0)
            
            let yscale = d3.scaleLinear()
                .domain([me.tstart, me.tend])
                .range([me.height0, 0]) // w/o -1, tooltip shows up at strange position somehow.

            let yaxis = d3.axisLeft()
                .scale(yscale)
                .ticks(5)


            function mmdd2date(mmdd) {
                let date = me.parse_date(me.yyyy + '/' + mmdd);
                return date;
            }

            svg.selectAll("bar").remove();
            let bars = svg.selectAll("bar").data(me.data)
            me.rects = [];
            bars.enter().each(function(day, i) {
                for (let tz of day.hist) { // for each time zone in a day
                    let r = d3.select(this).append("rect")
                    r.style("fill", function(){
                        let c;
                        switch (tz.color) {
                        case 'red':
                            c = '#ff4444';
                            break;
                        case 'yellow':
                            c = '#ffff44';
                            break;
                        case 'blue':
                            c = '#4444ff';
                            break;
                        default:
                            c = '#888888';
                            break;
                        }
                        return c;
                    })
                    r.attr("x", function() {
                        let date = mmdd2date(day.mmdd);
                        let xs = xscale(date);
                        return xs;
                    });
                    r.attr("width", function() { return me.barwidth; });
                    r.attr("y", function() { return yscale(me.tend); });
                    r.attr("height", function() { return yscale(tz.time); });
                    me.rects.push({
                        svgobj: r,
                        day: day,
                    });
                }
            })
                
            svg.append("g")
                .attr("class", "x-axis")
                .attr("transform", "translate(0,  " + me.height0 + ")")
                .call(xaxis)

            svg.append("g")
                .attr("class", "y-axis")
                .call(yaxis)

            svg.append("g")
                .append("text")
                .attr("class", "y-label")
                .attr("x", -yscale(12))
                .attr("y",  -30)
                .attr("text-anchor", "middle")
                .attr("transform", "rotate(-90)")
                .text('hour')
        },
    }; // prototype

    // export the constructor
    window.Barchart = Barchart;
}($)
