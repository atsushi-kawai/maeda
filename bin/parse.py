#!/usr/local/bin/python
# -*- encoding: utf-8 -*-                                                                                                                 
from __future__ import print_function

import sys
import os
import re
import json
import glob
import argparse
import datetime
import time

ROOTDIR = '/home/fd-tech/www/maeda'

def parse_argument():
    ap = argparse.ArgumentParser(description='parse the Maeda flag color logs.')
    ap.add_argument('-d', '--debug', action = 'store_true')
    args = ap.parse_args()
    return args

def parse_logfiles(files):
    res = []
    for file in files:
        with open(file, 'r') as f:
            lines = f.readlines()
        for line in lines:
            fs = line.split()
            (date, time) = fs[0].split('-')
            res.append({
                'date': date,
                'time': time[0:2],
                'color': fs[1],
            })
    return res;

def main(args=None):
    if args is None:
        args = parse_argument()

    files = [ 'flag201806.txt' ]
    path = '{root}/log/flag*.txt'.format(root=ROOTDIR)
    files = glob.glob(path)
    logs = parse_logfiles(files)
    res = logs

    if args.debug:
        print(json.dumps(res, indent=4, ensure_ascii=True))
    else:
        print(json.dumps(res, ensure_ascii=True))


if __name__ == "__main__":
    import sys
    sys.exit(main())
