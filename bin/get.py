#!/usr/local/bin/python
# -*- encoding: utf-8 -*-                                                                                                                 
from __future__ import print_function

import sys
import os
import re
import argparse
import datetime
import time

ROOTDIR = '/home/fd-tech/www/maeda'

def parse_argument():
    ap = argparse.ArgumentParser(description='wget Maeda site and parse the flag color.')
    ap.add_argument('-i', '--input', default = 'http://www.maedamisaki.jp/sea-status')
    args = ap.parse_args()
    return args

# execute 'cmd[]' as a child process and returns its output.                                                                              
#                                                                                                                                         
def exec_child(cmd):
    import subprocess, os
    proc = subprocess.Popen(cmd, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (sout, serr) = proc.communicate()
    return sout.rstrip(os.linesep).split(os.linesep)

# read lines from a file or stdin.                                                                                                        
# returns an array containing the lines as array elements.                                                                                
#                                                                                                                                         
def read_lines(fname):
    if fname:
        f = open(fname)
    else:
        f = sys.stdin
    lines = f.readlines()
    return lines



# <th id="flg"></th><td id="nowstatus" colspan="3" style="font-weight: bold;">禁止☓</td>

def parse_lines(lines):
    for line in lines:
        m = re.match(r'.*id="flg".*>([^<]+)</td>', line)
        if m: break
    note = m.group(1)
    if note == '禁止☓':
        color = 'red'
    elif note == '注意△':
        color = 'yellow'
    elif note == '可能○':
        color = 'blue'
    else:
        color = 'N/A (' + note + ')'
    return color

# for the page older than Jun 2019
def parse_lines0(lines):
    show = 0
    color = 'N/A'
    for line in lines:
        m = re.match(r'.*<TR\sbgcolor', line)
        if m: show += 1
        m = re.match(r'.*現在', line)
        if show > 0 and m: show += 1
        m = re.match(r'.*</TR>', line)
        if m: show = 0
        if show > 1:
            m = re.match(r'.*color="#([a-fA-F0-9]{6})">■', line)
            if m: color = m.group(1)
            if m and m.group(1) == 'ffff00': color = 'yellow (' + color + ')'
            if m and m.group(1) == 'ff0000': color = 'red (' + color + ')'
            if m and m.group(1) == 'cc0000': color = 'red (' + color + ')'
            if m and m.group(1) == '0000ff': color = 'blue (' + color + ')'
    return color

def main(args=None):
    if args is None:
        args = parse_argument()
    cmd = ['/usr/local/bin/wget', '-O', '/dev/stdout', args.input]

    lines = exec_child(cmd)
    color = parse_lines(lines)
    d = datetime.datetime.now()
    fname = ROOTDIR + '/log/flag' + d.strftime('%Y%m') + '.txt'
    with open(fname, 'a') as f:
        f.write('{0} {1}\n'.format(d.strftime('%Y/%m/%d-%H:%M'), color))

    import glob
    logfiles = glob.glob(ROOTDIR + '/log/flag*.txt')
    preamble = """<html lang="ja">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/default.css" type="text/css">
        <title>Cape Maeda Sea Condition</title>
    </head>
    <body>
        <h1>Cape Maeda Sea Condition</h1>
        <ul>
"""
    postamble = """        </ul>
    </body>
</html>
"""
    with open(ROOTDIR + '/raw.html', 'w') as f:
        f.write(preamble)
        for logfile in sorted(logfiles, reverse=True):
            logfile = os.path.basename(logfile)
            f.write("            <li><a href='./{0}'>{0}</a></li>\n".format('./log/' + logfile))
        f.write(postamble)
if __name__ == "__main__":
    import sys
    sys.exit(main())
